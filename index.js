require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
//const mongoString = process.env.DATABASE_URL;
const mongoString = 'mongodb+srv://hazedawn:hazedawn@cluster0.m4jpp.mongodb.net/test'

mongoose.connect(mongoString, {
    useUnifiedTopology:true,
    useNewUrlParser: true,
});
const database = mongoose.connection;

const userRoutes = require('./routes/user_routes');

const teamRoutes = require('./routes/team_routes');

const teamMemberRoutes = require('./routes/team_member_routes');

const coachRoutes = require('./routes/coach_routes');

const playerRoutes = require('./routes/player_routes');

const activityRoutes = require('./routes/activity_routes');


database.on('error', (error) => {
    console.log(error);
})

database.once('connected', () => {
    console.log('Database Connected');
})
const app = express();

app.use(express.json());

app.listen(3000, () => {
    console.log(`Server Started at 3000`);
});

app.get('/', (req, res) => {
    res.send('Sport in one is ready!');
});

app.use('/user', userRoutes);

app.use('/coach', coachRoutes);
app.use('/player', playerRoutes);

app.use('/team', teamRoutes);
app.use('/team', teamMemberRoutes);

app.use('/activity', activityRoutes);
