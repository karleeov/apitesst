const express = require('express');

const router = express.Router();

const auth = require('../middleware/auth');

const Team = require('../model/team');
const TeamMember = require('../model/team_member');
const User = require('../model/user');

module.exports = router;

// Get all team members
router.get('/:teamId/member', async (req, res) => {
  try {
    const teamId = req.params.teamId;
    const team = await Team.findById(teamId);
    if (!team) {
      res.json({
        code: 404,
        message: 'Team not found',
      });
      return;
    }

    const teamMembers = await TeamMember.find({
      team: team._id,
    })
      .populate({
        path: 'user',
        select:
          'chineseName englishName nickname role playerProfile',
      })
      .select('isAdmin createdAt updatedAt -_id');

    res.json({
      code: 200,
      data: teamMembers,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: error.message });
  }
});

// For coach or team admin to add a member
router.post('/:teamId/member', async (req, res) => {
  // TODO Add authorization check on current user: Only team admin can add member

  try {
    const teamId = req.params.teamId;
    const {
      // User ID of the target member
      userId,
    } = req.body;

    const team = await Team.findById(teamId);
    if (!team) {
      res.json({
        code: 404,
        message: 'Team not found',
      });
      return;
    }

    const user = await User.findById(userId);
    if (!user) {
      res.json({
        code: 404,
        message: 'User not found',
      });
      return;
    }

    let member = await TeamMember.findOne({
      team: team._id,
      user: user._id,
    });
    if (member) {
      res.json({
        code: 400,
        message: 'This user is already in the team.',
      });
      return;
    }

    member = await TeamMember.findOne({
      user: user._id,
    });
    if (member) {
      res.json({
        code: 400,
        message: 'This user is already in another team. Forbid adding into the specified team.',
      });
      return;
    }

    member = new TeamMember({
      team: team._id,
      user: user._id,
      isAdmin: false,
    });

    await member.save();

    team.members.push(member.id);
    await team.save();

    res.json({
      code: 200,
      data: member,
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});
