const express = require('express');

const router = express.Router();

const auth = require('../middleware/auth');

const Activity = require('../model/activity');

module.exports = router;

// [TODO TBC] Create a new activity
router.post('/post', async (req, res) => {
  const { hostTeam, name, venue, type, isPinned, startedAt, endedAt } =
    req.body;

  const activity = new Activity({
    name,
    venue,
    type,
    isPinned: isPinned || false,
    startedAt,
    endedAt,
  });

  try {
    await activity.save();

    res.status(200).json(activity);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// [TODO TBC] Get all Method
router.get('/getAll', async (req, res) => {
  try {
    const Team = await Activity.find();
    res.json(Team);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// [TODO TBC] Get by ID Method
router.get('/getOne/:id', async (req, res) => {
  try {
    const data = await Activity.findById(req.params.id);
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// [TODO TBC] Update by ID Method
router.patch('/update/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const updatedData = req.body;
    const options = { new: true };

    const result = await Activity.findByIdAndUpdate(id, updatedData, options);

    res.send(result);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// [TODO TBC] Delete by ID Method
router.delete('/delete/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Activity.findByIdAndDelete(id);
    res.send(`Document with ${data.name} has been deleted..`);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});
