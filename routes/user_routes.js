const express = require('express');

const router = express.Router();

const auth = require('../middleware/auth');

const User = require('../model/user');

module.exports = router;

// [TODO TBC]
router.post('/post', async (req, res) => {
  const user = new User({
    role: req.body.role,
    chineseName: req.body.chineseName,
    englishName: req.body.englishName,
    firebaseUserId: req.body.firebaseUserId,
    email: req.body.email,
    dateOfBirth: req.body.dateOfBirth,
  });

  try {
    await user.save();

    res.status(200).json(user);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// [TODO TBC] Get all Method
router.get('/getAll', async (req, res) => {
  try {
    const user = await User.find();
    res.json(user);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// [TODO TBC] Get by ID Method
router.get('/getOne/:id', async (req, res) => {
  try {
    const data = await User.findById(req.params.id);
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// [TODO TBC] Update by ID Method
router.patch('/update/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const updatedData = req.body;
    const options = { new: true };

    const result = await User.findByIdAndUpdate(id, updatedData, options);

    res.send(result);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// [TODO TBC] Delete by ID Method
router.delete('/delete/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const data = await User.findByIdAndDelete(id);
    res.send(`Document with ${data._id} has been deleted..`);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});
