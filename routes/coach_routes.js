const express = require('express');

const router = express.Router();

const auth = require('../middleware/auth');

const Role = require('../model/enum/Role');

const User = require('../model/user');

module.exports = router;

// Register a user as a coach
router.post('/', async (req, res) => {
  try {
    let user = await User.findOne({
      firebaseUserId: req.body.firebaseUserId,
    });

    if (!user) {
      // User not found

      // Create a new user
      user = new User({
        firebaseUserId: req.body.firebaseUserId,
        email: req.body.email,
        role: Role.Coach,
        chineseName: req.body.chineseName,
        englishName: req.body.englishName,
        nickname: req.body.nickname,
      });

    } else {
      // User already exists

      if (!user.role) {
        user.role = Role.Coach;
      }
    }

    await user.save();

    res.status(200).json({
      message: 'Done',
      data: user,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: error.message });
  }
});
