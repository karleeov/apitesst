const express = require('express');

const router = express.Router();

const auth = require('../middleware/auth');

const Role = require('../model/enum/Role');

const Team = require('../model/team');
const TeamMember = require('../model/team_member');
const User = require('../model/user');

module.exports = router;

// Create a team
router.post('/', async (req, res) => {
  const {
    // The current user (expecting a coach) who is creating a team.
    // TODO Add verification with firebase.
    userId,

    chineseName,
    englishName,
    nature,
  } = req.body;

  const user = await User.findById(userId);
  if (!user) {
    res.json({
      code: 404,
      message: 'User not found',
    });
    return;
  } else if (user.role !== Role.Coach) {
    res.json({
      code: 400,
      message: 'Only coach can create team.',
    });
    return;
  }

  const teamMember = await TeamMember.findOne({
    user: user._id,
  });
  if (teamMember) {
    res.json({
      code: 400,
      message: 'This user is already in a team. Forbid creating another team.',
    });
    return;
  }

  try {
    // Create a team
    const team = new Team({
      creator: user._id,
      chineseName,
      englishName,
      nature,
    });
    await team.save();

    // Create a team member
    const teamMember = new TeamMember({
      team: team._id,
      user: user._id,
      isAdmin: true, // such flag is important
    });
    await teamMember.save();

    team.members.push(teamMember);
    await team.save();

    res.status(200).json({
      code: 200,
      message: 'success',
      data: team,
    });
  } catch (error) {
    console.error(error);
    res.status(400).json({ message: error.message });
  }
});

// [TODO TBC] Get all Method
router.get('/getAll', async (req, res) => {
  try {
    const team = await Team.find();
    res.json(team);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// [TODO TBC] Get by ID Method
router.get('/getOne/:id', async (req, res) => {
  try {
    const data = await Team.findById(req.params.id);
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// [TODO TBC] Update by ID Method
router.patch('/update/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const updatedData = req.body;
    const options = { new: true };

    const result = await Team.findByIdAndUpdate(id, updatedData, options);

    res.send(result);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// [TODO TBC] Delete by ID Method
router.delete('/delete/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Team.findByIdAndDelete(id);
    res.send(`Document with ${data.name} has been deleted..`);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});
