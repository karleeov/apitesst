const mongoose = require('mongoose');

const activitySchema = new mongoose.Schema(
  {
    hostTeam: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'team',
    },
    name: {
      required: true,
      type: String,
    },
    venue: {
      required: true,
      type: String,
    },
    type: {
      required: true,
      type: String,
    },
    isPinned: {
      required: true,
      type: Boolean,
    },
    startedAt: {
      required: true,
      type: String,
    },
    endedAt: {
      required: true,
      type: String,
    },
    // comments: [
    //   {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'activity_comment',
    //   },
    // ],
  },
  {
    timestamps: true,
  }
);

const Activity = mongoose.model('activity', activitySchema);

module.exports = Activity;
