const mongoose = require('mongoose');

const teamMemberRequestSchema = new mongoose.Schema(
  {
    team: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'team',
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    status: {
      required: true,
      type: String,
    },
    approver: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
  },
  {
    timestamps: true,
  }
);

const TeamMemberRequest = mongoose.model(
  'team_member_request',
  teamMemberRequestSchema
);

module.exports = TeamMemberRequest;
