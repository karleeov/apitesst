const mongoose = require('mongoose');

const matchSchema = new mongoose.Schema(
  {
    hostTeam: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'team',
    },
    opponentTeam: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'team',
    },
    name: {
      required: true,
      type: String,
    },
    venue: {
      required: true,
      type: String,
    },
    startedAt: {
      required: true,
      type: String,
    },
    hostTeamScore: {
      required: false,
      type: Number,
    },
    opponentTeamScore: {
      required: false,
      type: Number,
    },
  },
  {
    timestamps: true,
  }
);

const Match = mongoose.model('match', matchSchema);

module.exports = Match;
