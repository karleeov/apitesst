const mongoose = require('mongoose');

const teamMemberSchema = new mongoose.Schema(
  {
    team: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'team',
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    isAdmin: {
      required: true,
      type: Boolean,
    },
    jerseyNumber: {
      required: false,
      type: Number,
    },
    playerPosition: {
      required: false,
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const TeamMember = mongoose.model('team_member', teamMemberSchema);

module.exports = TeamMember;
