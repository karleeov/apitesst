const Role = {
  Unspecified: '',
  Coach: 'coach',
  Player: 'player',
};

module.exports = Role;
