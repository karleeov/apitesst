const JerseySize = {
  Unspecified: '',
  Small: 'S',
  Medium: 'M',
  Large: 'L',
  ExtraLarge: 'XL',
  ExtraExtraLarge: 'XXL',
};

module.exports = JerseySize;
