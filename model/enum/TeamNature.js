const TeamNature = {
  PrimarySchool: 'P',
  SecondarySchoolAGrade: 'SA',
  SecondarySchoolBGrade: 'SB',
  SecondarySchoolCGrade: 'SC',
  TertiarySchool: 'T',
};

module.exports = TeamNature;
