const PlayerPosition = {
  Unspecified: '',
  Center: 'C',
  PowerForward: 'PF',
  SmallForward: 'SF',
  ShootingGuard: 'SG',
  PointGuard: 'PF',
};

module.exports = PlayerPosition;
