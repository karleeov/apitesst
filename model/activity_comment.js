const mongoose = require('mongoose');

const activityCommentSchema = new mongoose.Schema(
  {
    activity: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'activity',
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    content: {
      required: false,
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const ActivityComment = mongoose.model(
  'activity_comment',
  activityCommentSchema
);

module.exports = ActivityComment;
