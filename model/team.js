const mongoose = require('mongoose');

const teamSchema = new mongoose.Schema(
  {
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    chineseName: {
      required: true,
      type: String,
    },
    englishName: {
      required: true,
      type: String,
    },
    logoUrl: {
      type: String,
    },
    nature: {
      required: true,
      type: String,
    },
    members: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'team_member',
      },
    ],
    activities: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'activity',
      },
    ],
    hostingMatches: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'match',
      },
    ],
  },
  {
    timestamps: true,
  }
);

const Team = mongoose.model('team', teamSchema);

module.exports = Team;
