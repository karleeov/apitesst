const mongoose = require('mongoose');

var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

const userSchema = new mongoose.Schema(
  {
    firebaseUserId: {
      required: true,
      trim: true,
      type: String,
      unique: true,
    },
    email: {
      lowercase: true,
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        'Please fill a valid email address',
      ],
      required: 'Email address is required',
      trim: true,
      type: String,
      unique: true,
      validate: [validateEmail, 'Please fill a valid email address'],
    },
    dateOfBirth: {
      type: Date,
      required: false,
    },
    role: {
      required: true,
      type: String,
    },
    chineseName: {
      required: true,
      type: String,
    },
    englishName: {
      required: true,
      type: String,
    },
    nickname: {
      required: false,
      type: String,
    },
    avatar: {
      required: false,
      type: String,
    },

    playerProfile: {
      height: {
        type: Number,
      },
      weight: {
        type: Number,
      },
      jerseySize: {
        type: String,
      },
    },
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model('user', userSchema);

module.exports = User;
